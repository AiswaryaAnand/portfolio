export const projects = [
    {
      title: "Business Analytics and Digital Media",
      subtitle: "Business Technology Management - IBM",
      description:
        "Lorem ipsum dolor sit amet consectetur adipisicing elit. Praesentium dolore rerum laborum iure enim sint nemo omnis voluptate exercitationem eius?",
      image:'assets/ba.jpeg',
      link: "https://www.coursera.org/account/accomplishments/certificate/J5MD6WLLV6F8",
    },
    {
      title: "Digital Transformations",
      subtitle: "Business Technology Management - IBM",
      description:
        "Lorem ipsum dolor sit amet consectetur adipisicing elit. Praesentium dolore rerum laborum iure enim sint nemo omnis voluptate exercitationem eius?",
      image:'assets/dt.jpeg',
      link: "https://www.coursera.org/account/accomplishments/certificate/X7RLU3LV93Y8",
    },
    {
      title: "Neural Networks and Deep Learning",
      subtitle: "Deep Learning Specialisation - Deeplearning.Ai",
      description:
        "Lorem ipsum dolor sit amet consectetur adipisicing elit. Praesentium dolore rerum laborum iure enim sint nemo omnis voluptate exercitationem eius?",
      image:'assets/nn.png',
      link: "https://www.coursera.org/account/accomplishments/certificate/XCWLHZCC8PYC",
    },
    {
      title: "Introduction to Computer Vision and Image Processing",
      subtitle: "Machine Learning - IBM",
      description:
        "Lorem ipsum dolor sit amet consectetur adipisicing elit. Praesentium dolore rerum laborum iure enim sint nemo omnis voluptate exercitationem eius?",
      image:'assets/cv.png',
      link: "https://www.coursera.org/account/accomplishments/certificate/PDE9DG4AJNDG",
    },
    {
      title: "Social Media Strategist",
      subtitle: "Krypto Cards",
      description:
        "Lorem ipsum dolor sit amet consectetur adipisicing elit. Praesentium dolore rerum laborum iure enim sint nemo omnis voluptate exercitationem eius?",
      image:'assets/kc.jpg',
      link: "https://www.kryptocards.tech/",
    },
    {title: "Front End Develeopment with React",
    subtitle: "Web Development - HKUST",
    description:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Praesentium dolore rerum laborum iure enim sint nemo omnis voluptate exercitationem eius?",
    image:'assets/wd.png',
    link: "https://www.coursera.org/account/accomplishments/certificate/LPR9Y885D4YF",
    },
  ];

  export const skills = [
      "Python" , "C++" ,"C" , "React" , "Computer Vision & Image Processing" , "Basic Neural Networks" , "Digital Marketing" , "Debating" , "Problem Solving"
  ];
